const mysqlssh = require('mysql-ssh');
const async = require('async');

const AWS = require('aws-sdk')
const ses = new AWS.SES({region: 'eu-west-1'})
 
// const RECEIVER = 'lamia.ebada@robustastudio.com'
const RECEIVER = 'ahmed.abdel-wahab@robustastudio.com'
const SENDER = 'accounts@robustastudio.com'
const QUERY = `
SELECT 
  users.email,
  time_entries.started_at,
  time_entries.ended_at
FROM
  tasks
    INNER JOIN
  timelogs ON tasks.id = timelogs.task_id
    INNER JOIN
  time_entries ON timelogs.id = time_entries.timelog_id
    INNER JOIN
  users ON users.id = tasks.user_id
WHERE
  DATE(time_entries.ended_at) >= STR_TO_DATE(?, '%d/%m/%Y')
  AND
  DATE(time_entries.ended_at) <= STR_TO_DATE(?, '%d/%m/%Y')
ORDER BY time_entries.ended_at DESC;
`;
const PRIVATE_SSH_KEY = `
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAqrdxrakyAlCfXKkYfkkjjOlbTn7/ep+2eDRwJjaOn9BmjPvG
c4LSn8daU6ujNGMxBPL7Lq1AKrbxo7LePpyXJCdEakKzJP0orUqJoA/QvHlr/jb/
840XnfmxJnGO+XpRIa0XUnOXPJBj/v72z1xD/SriyvesKBTo41DOrpySoLa3hAuT
d3waAX14smBYRcWB1iZFXGU74amqShgqLbv9pzLFIcTHHcGeVycCNSP1pwwUTCj7
SysmCdtiUzF3Y9qv+mpF5pdXnsAGICW6gvJrSKlLm87ssR4tNXidXWTcmYDP9yJv
kBR4MzoBJK4WKY09YUVTwBI8GEOcAmod1YLsgGvzJCgTuCESFAMMqMJwcE9qgzlt
XZPi3aP9dU6akjk4SazQWNfcBBjJX0Tl0Ww45vPdBN0R7BANWBfpzTI1FM/CgkWi
KH0D+dt0aDRc6z6jw/nSM8/Q5Q4hPtCBqtha2DhmMLDbyc1HDQPgEZSNcNCzG7ZT
ZIRSIB8sK74QPHCpppnefOLcCVwq88JcYnXSlzyhibeL+hWFI5XSTYVmJhRJ/d65
SJCt/XJn8vAkBUGgWiSpiUWULEMBumndBH1kSx2UNV4i3eAgym2Fxpcs0Nnol3xD
B96J8YrLNZXVlHPseHM0AlXS6gKKdcsMUuBzzKf8ajlU+E6YIngckeoK8BkCAwEA
AQKCAgAJugX6FQfKZfmOd/vzRguGfc07/bRksjDdTG+PXdBmfqM5KSc3cZ66h8s3
tTHJTFWAqK9zC1/6jw3Ze9l5y8Y4wxv8yewXEW194h9RRVVpR6nFhDC1Gbbcw1s6
i4HP6Q4yUwBfLELTqo5n+vLpAaLGC+l8Evx2FCrouLPt8vHPwllWZqpyRlnc998m
mqKs5wHsx6VwDDyLMPWI3zOYA3wCSLw+nk2R/nQdFxXjHTdj2yZU6IjY0Y6LRTqm
7syUneMk62UsLDdLzjkLH9suls1SCINMT0/hD40X3NVqetKu1E4FEKs2lFP85zMl
ed29ZFS49BA/g0p1P3TmFlG41orO9npxf7OKo2XtTcBelYxwzCy9orffYiZgy7vY
3cZNahsv2qPYpJA5y/QOvEH4zohSjiN1ZBWEK2M3wU6qBNQ5s/AR9g5hkMIgf5WS
Ogx7ORnIVwWXTBnf4+RAW9hcQgCdFC9SLlo1if7gnNapVAxIvzHUYgXKJ/spV6xT
lZdEdc1k4nIMIRVv8ZRvfSqRxJPc3C/14ieZIT7OXENmAgjI+hpMUVh6c9AClW7o
OzWMtUxID5IoTMQQ6/21iHOfCyXfpiyKJpfa72nWRhVt9J5xheGC48RFmX0TNuZW
EHQtoZTAul422TRJsbRaoP1zb2sN1M2unB6kJDrVqcFYe0gxYQKCAQEA3cgdYOQr
zn05WY4sAINriRoBovnSikacajDRCPH9EkxXPUreWzx65VWGxO4Z14QpWPTWY1sJ
v17K9slV1lWXNPhnoq7onH5Xc25BbyQXSVSUn53aOYiP0vD1c1vEWB3gdP7AuWdP
9e9wj+eUtpfMBK1A7GGr7ebcEKYdy/tj4UQ1Vw2ewbPOsWINvITs+uetmneIIlK+
wi/cGJ3YdJW3UD6aZbb1FvaMc/SiKRSUCss2jKOwkVjHI2qepm+l++5YV2VXMCHV
QQ33E2lNIJbnF6Yfikb/N2PqI+RVhqAoBNa9s3BR0vOOL00ZYoIt9iXPH38ERVJi
0T+vVnLsr1DKZQKCAQEAxQ5ezFMs9fzg/dgjxfgZ/F7qqE95WQSslPBjcOhl9I7Y
q1NXoXLPh8pR9xM0dg0bf+TJLSPgMkiYEBhsJmFOJodVlrkjJCR362Dvz95hOARb
T+zjr7TLTmAfrFpo4ClJKCicsIm8mpH7W8Ae5G5A35ddvzy2aM4ssHuvM9cAhn5Z
0yM8hsqk3TfkIWX/es5LHwXNVTmOFlHg2enYpwqjftly2rhLxuHrwMg2/ckbLs/V
25tzlaEog16C3YmmcKtertwpypfCVG2ekR20se41JtVoFsoqiZ8xf8BHZHrMtSBE
M7OzSx/hPeobdY92cOvddQ1pq0BoCzIzd9dKI8g5pQKCAQEAzT5mwcYnLD8DFffW
jNsSZ5zjqtmbmE/kbPmkWazo0UhI6/YfdTuxWA8gW5sl2yb5xNNAnE4h5TJqjpyg
jWxENMc10X4g0sKxV6fPcnVp/3kA2f6Rd/EDeOl5nTptqEZt9JfV7z7G5wihmOtt
2tT7/8/+Z0O0gsPFILs6lA2fz1b6LgfyxbwLKmld4Pr6/O/u3yaDm5AL7VWxUC6z
pKDxNpfY9wysIIYmUSEx+OaMYrjRULIjsz7/e60uCUUFzPI0Wxzz5R1KZ5vu7c3j
jKdaUa5xPdyaT75tbq3e4GnS3zeD802cy6aODQRC28Exm7w9xB4d67FktdVEBLLy
iwQvaQKCAQAv3Xi7auy956oDHvA5kzVErw2SypMwQ0lkeCu/bkPtrpEEtJ7RkaDF
XCiiumCA2Fg4rasMMu2vaSC+sB/9Bp/EDhWFIf1eNxmF7Cr0NBErWSuQ83qu4FyN
8h1fcSqOYJjrj0nYO+y7IwCKXdVjKUDKzuqvnCC2vnXufGynS+9Nu/03oxR9rVV0
grIARtVS0C9BHtisfel8lHDonbvJf3UNR+Z1KFUHvzLTxg5NgP8AVO2ehVkDHH1J
w7hH9bqfHZFHKiVgl5JVjbb5itysZ68zepzpBDxm52tPm7X7uzRkPq0yXdEMS3cb
4fS/jpR3g2NsnlBvj5p2NcB4uKvCm00dAoIBAQDF+5r0xB6e8yJ6jObkK9vCgUQk
X6DFrPWD3AXmP6kXWdHGpWtoGAcUMPEUXThGcNnQWr2yyBL1IMrPANj89YbDnGJj
YJAKTHLo1gK/8U1Rc+uGbgy1if4XhZC79SWoGz0fBal1IioFvODWhytSMUbyidCg
UnPImEy8ay9PaZ2mwUyn3mW13SJ1qyr6fR8MdIi4MYmVks1oeBoyTR0Kn1dQzKZZ
e5fMErKZ43uaxoZhIkSj5aJtsT6ifDiPZHJIwsClBRtKC3B98Gaw8m2q/fQZL6UQ
jLWrbpiEVuGwDP1eOlmdLPw9fBQJvM3KhFo0lje6FCMgIM4a1pyYfOwJTpsj
-----END RSA PRIVATE KEY-----
`
exports.handler = function (event, context, callback) {
  console.log('Received event:', event);
  const {startDate, endDate} = (event.queryStringParameters) || {startDate: '19/9/2017', endDate: '19/9/2017'};
  callback(null, {
    statusCode: 200
  });
  async.waterfall([
    nextFunc => mysqlConnect(nextFunc),
    (mysqlClient, nextFunc) => getTimeEntries(mysqlClient, startDate, endDate, nextFunc),
    (dataObj, nextFunc) => sendEmail(dataObj, startDate, endDate, nextFunc),
  ], err => {
    if (err) {
      console.error('Error at the end', err);
    }
    // process.exit(0);
  });
}

function getTimeEntries(mysqlClient, startDate, endDate, callback) {
  mysqlClient.query(QUERY, [startDate, endDate], (err, results, fields) => {
    err && console.error('Error fetchign stuff:', err);
    setImmediate(() => mysqlssh.close());
    results = results.reduce((acc, result) => {
      if (!acc[result.email]) {
        acc[result.email] = 0;
      }
      acc[result.email] += new Date(result.ended_at).getTime() - new Date(result.started_at).getTime(); 
      return acc;
    }, {});
    Object.keys(results).forEach(key => {
      results[key] = results[key] / (60 * 60 * 1000);
    });
    callback(err, results);
  });
}

function mysqlConnect(callback) {
  mysqlssh.connect(
    {
      host: '178.79.158.14',
      user: 'reader',
      port: '6311',
      privateKey: PRIVATE_SSH_KEY,
      // debug: console.log.bind(console, 'Debug SSH')
    },
    {
      host: '127.0.0.1',
      user: 'reader',
      password: 'rob@123',
      database: 'hr_tool_production'
    }
  )
  .then(client => {
    callback(null, client);
  })
  .catch(err => {
    if(err) {
      console.error('Error Connecting to mysql', err);
    }
    callback(err, null);
  });
}

function sendEmail (data, startDate, endDate, done) {
  const params = {
    Destination: {
      ToAddresses: [
        RECEIVER
      ]
    },
    Message: {
      Body: {
        Text: {
          Data: JSON.stringify(data, null, 2),
          Charset: 'UTF-8'
        }
      },
      Subject: {
        Data: `[Toggl Report] Weekly report starting from ${startDate}`,
        Charset: 'UTF-8'
      }
    },
    Source: SENDER
  }
  ses.sendEmail(params, done)
}
